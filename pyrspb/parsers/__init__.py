from .book import VersionScraper
from .invention import MaterialScraper, MaterialSourceScraper
